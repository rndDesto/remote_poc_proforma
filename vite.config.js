import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import federation from "@originjs/vite-plugin-federation";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    federation({
      name: "remote_proforma",
      filename: "remoteEntry.js",
      exposes: {
        "./ProformaPage": "./src/Pages/Proforma",
        "./KontainerPage": "./src/Pages/Kontainer",
      },
      shared: ["react", "react-dom", "@mantine/core", "@mantine/hooks","@tanstack/react-location","@logee-fe/services","@logee-fe/utils"],
    })
  ],
  server: {
    port:4101,
  },
  preview: {
    port: 5001,
  },
  build: {
    modulePreload: false,
    target: "esnext",
    minify: false,
    cssCodeSplit: false,
  }
})
