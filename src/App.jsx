import { useState } from 'react'
import { Proforma } from './Pages'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <p>REMOTE PROFORMA</p>
      <Proforma />
       <button onClick={() => setCount((count) => count + 1)}>
          count is ss {count}
        </button>
    </>
  )
}

export default App
