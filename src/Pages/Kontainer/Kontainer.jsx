import { Button, MantineProvider } from "@mantine/core"
import { useState } from "react"

const Kontainer = (props) => {
  const {goTo} = props

  const [count,setCount] = useState(0)
  return (
    

    <MantineProvider withGlobalStyles withNormalizeCSS>
      <div onClick={()=>setCount((prev)=>prev+1)}>Kontainer {count}</div>
      <Button onClick={()=>goTo('mantul')}>Buttonny mantine</Button>
    </MantineProvider>
  )
}

export default Kontainer