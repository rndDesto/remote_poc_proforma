import { useEffect, useState } from "react";
import styles from "./Proforma.module.css";
import { apiClient, useFetch } from "@logee-fe/services";
import { useLocation, useMatch, useSearch } from "@tanstack/react-location";
import { useLocalStorage } from "@mantine/hooks";
import { setCookie } from "@logee-fe/utils";

const instance = apiClient({ 
  baseURL: 'https://dev-api.ecologee.id', // env base url
  headers: {
    Authorization: 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJmZWF0dXJlIjoiQkFTSUMiLCJhc3NpZ25JbmRpY2FyIjpmYWxzZSwic2hvd1RyYWNraW5nIjpmYWxzZSwiZW1haWwiOiJ0ZXN0LWRldi5ucGN0MUB5b3BtYWlsLmNvbSIsInVzZXJJZCI6IjRjMzExOWUwLWQ1Y2UtNDFjMS04ZGU2LTdjMThjMmJjN2IwZCIsIm5hbWUiOiJEZXRhIHRlc3QgMSIsImF2YXRhciI6Imh0dHBzOi8vZGV2LW1pbmlvLmxvZ2VldHJhbnMuY29tL2xvZ2VlLXRyYW5zL2F2YXRhci80MWJlZWJhZi0xZWY3LTQzNmUtOTkxZS0xOWQyMDg3NmM1MjgtZTE0M2IwNjEtMzhlMS00NGMzLWI2MjItNmY5Nzk3NjZhNGQyLmpwZyIsInJvbGVzIjpbInBvcnQta29qYSIsInBvcnQtbnBjdDEiLCJjYXJnby1vd25lciIsImNhcmdvLWFkbWluIl0sImFwcCI6InBvcnQtc3NvLWRhc2hib2FyZCIsInBsYXRmb3JtIjoibG9nZWV0cnVjayIsInBsYXRmb3JtTmFtZSI6IkxvZ2VlIFRydWNrIiwicGhvbmUiOiIwODEzNDQ2Njc3ODgiLCJ3ZWJBcGlLZXkiOiI5Njg3NzRiMzZjZTMyNzIxNjM2MzI4N2ViMzU1ZTIxNiIsImRldmljZUlkIjoiZHVtbXktRGV2aWNlSUQiLCJjb21wYW55Ijp7ImlkIjoiYjAxODk0ZGQtM2QzOS00MjhiLTg4YzgtNTVjZmI3NGQ4ZjBkIiwibGF0IjowLCJsb25nIjowLCJjb21wYW55SWQiOiJiMDE4OTRkZC0zZDM5LTQyOGItODhjOC01NWNmYjc0ZDhmMGQiLCJuYW1lIjoiTlBDVE9ORSIsImFkZHJlc3MiOiJKbC4gVGVybWluYWwgS2FsaWJhcnUgUmF5YSBLYXYuQiBOby4xLCBLYWxpIEJhcnUsIENpbGluY2luZywgS290YSBKa3QgVXRhcmEsIERhZXJhaCBLaHVzdXMgSWJ1a290YSBKIiwiZW1haWwiOiJjb21tZXJjaWFsQG5wY3QxLmNvLmlkIiwibnB3cE51bSI6IjAwMDAwMDAwMDAxMDEyNCIsImlzQWN0aXZlIjp0cnVlfSwiaWF0IjoxNjgzMzg0NjE2LCJleHAiOjE2ODU5NzY2MTYsImF1ZCI6Ijk3YjMzMTkzLTQzZmYtNGU1OC05MTI0LWIzYTliOWY3MmMzNCIsImlzcyI6InRlbGtvbWRldiJ9.BapSyZU0JVEeGhvQzMNySchU2vFfIHMp-QfmYJBhxeEpuQ92LWAKN75L-HPbPUuoR9VWzNAKBmY1ICnjjV4o0_prmyKlPQSe3rm3rYKz_wmR2Pqrt_e37AKd6mzJYzEIYJfyJAJaJPK1Q4u_7M9KTmke0Ms0sAI0UoxDwa9Zn3NH4vZZGrO2hmo3k72zcopNt_1a1i3Ly-0EA5SFoZjcAXOSpntpJxh6eXCQVsYOO3VFUDcRFnjy75R28plHe2Y2LaEKSJ1iot0AgrwVAW-_-2gt-i2Th8wmFeSMppnxszy43-QZXeiufgCYWtuGMZ5DjfoKZziN9YP31HkkAY8Fl-MYqRJdxMAawBsZIVq45zth6mxKVbmfhwRbJRaBl0YXDFeFQgXzyepPBjl5tDfQovj8BJL_rJK0jai3YpLIeID9OIKOHd1i0g67sXk8o5CRy2q_SnjJPUEHjHkEh4eMd5k7CBVfoxLcCVgkxapmkIY_ryhHXBvxJDJL6DpVoFOj_sAOkCUc6YFedHXGPdhif6pYVsZtlDVYmyoR1YP8igrMskYhcx7i4wXxPQpqdnVbGxKOb_sQ1DLSEDE9jWiPpnZlwtkvY63p-tW4_2r3k12omOgIsWqdzO9qW5vRr7VKaN2TuA1THq9tG3OC9zRUypXJ9v82zg_oLHFnMqd02QY'
  },
});

const generateRandomString = (length) =>{
  let result = "";
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;

  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}


const Proforma = () => {
  
  const [count, setCount] = useState(0);
  const [value, setValue] = useLocalStorage({ key: 'color-scheme', defaultValue:generateRandomString(8) });
  const [errMsg,setErrMsg] = useState('')

  const search = useSearch()
  const location = useLocation()
  const match = useMatch()

  console.log("match = ", match);
  console.log("location = ", location);
  console.log("search = ", search);
  
  const onError =(err)=>{
    setErrMsg(err?.response?.data?.message)
  }

  const onSuccess =(s)=>{
    setErrMsg('')
  }

  const { data, loading, callFetch } = useFetch({ 
    body: null,
    method:'get',
    instance,
    onError:(err)=>onError(err),
    onSuccess:(s)=>onSuccess(s),
    url: '/npct1/v2/proforma-details'
   });

   useEffect(()=>{
    callFetch()
   },[])

  return (
    <div className={styles.root}>
      {loading && <p>memuat</p>}
      {errMsg && <p>{errMsg}</p>}
      <div onClick={() => setCount((count) => count + 1)}>Proforma {count}</div>

      <button
        onClick={() =>
          setValue(generateRandomString(8))
        }
      >
        ganti token
      </button>

      <button
        onClick={() =>
          setCookie('tokenCookie',generateRandomString(8))
        }
      >
        set cookie
      </button>
      <div> token host is {value}</div>
      <div className={styles.consoleLookLike}>
        <pre>{JSON.stringify(data,null,2)}</pre>
      </div>
    </div>
  );
};

export default Proforma;



